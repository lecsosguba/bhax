#include <iostream>
using namespace std;

typedef int (*v) (); //Egeszre mutato mutatot visszaado fuggvenyre mutato mutato
typedef int (*(*G) (int)) (); //Fuggvenymutato egy egeszet visszaado es ket egeszet kapo fuggvenyre mutato mutatot visszaado, egeszet kapo fuggvenyre

int *h() //Egeszre mutato mutatot visszaado fuggveny
{
	int a = 5;
	int *b = &a;
	cout << b;
}
int main ()
{
	//Egesz
	cout << "Egesz:" << endl;
	int a = 5;
	cout << a << endl;

	//Egeszre mutato mutato
	cout << "Egeszre mutato mutato:" << endl;
	int *b = &a;
	cout << b << endl;

	//Egesz referenciaja
	cout << "Egesz referenciaja:" << endl;
	int &r = a;
	cout << r << endl;

	//Egeszek tombje
	cout << "Egeszek tombje:" << endl;
	int c[5]={1,2,3,4,5};
	for(int i=0; i < 5; i++)
		cout << c[i] << ", ";
	cout << endl;

	//Egeszek tombjenek a referenciaja
	cout << "Egeszek tombjenek a referenciaja:" << endl;
	int (&tr)[5] = c;
	cout << tr << endl;

	//Egeszre mutato mutatok tombje
	cout << "Egeszre mutato mutatok tombje:" << endl;
	int *d[5];
	for(int i=0; i < 5; i++)
	{
		d[i]=&c[i];
		cout << d[i] << ", ";
	}
	cout << endl;

	//Egeszre mutato mutatot visszaado fuggveny
	cout << "Egeszre mutato mutatot visszaado fuggveny:" << endl;
	cout << h() << endl; 
	
	//Egeszre mutato mutatot visszaado fuggvenyre mutato mutato
	cout << "Egeszre mutato mutatot visszaado fuggvenyre mutato mutato:" << endl;
	int *(*mut)()= h;
	cout << reinterpret_cast<void*>(mut) << endl;
	
    return 0;
}

